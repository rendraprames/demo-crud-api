package com.rendra.demoAPI.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rendra.demoAPI.model.*;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{

}
