package com.rendra.demoAPI.controller;

import com.rendra.demoAPI.model.User;
import com.rendra.demoAPI.repository.UserRepository;
import com.rendra.demoAPI.exception.ResourceNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@CrossOrigin(origins = "http://localhost:8083")
@RestController
@RequestMapping("/api/v1/")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    // Read All Users
    @GetMapping("/users")
    public List<User> getAllEmployees(){
        return userRepository.findAll();
    }

    // Create User
    @PostMapping("/users")
    public User createUser(@RequestBody User user) {
        return userRepository.save(user);
    }

    // get user by id
    @GetMapping("/users/{users_Id}")
    public ResponseEntity<User> getEmployeeById(@PathVariable Long users_Id) {
        User user = userRepository.findById(users_Id)
                .orElseThrow(() -> new ResourceNotFoundException("User not exist with user_Id:" + users_Id));
        return ResponseEntity.ok(user);
    }

    // update user
    @PutMapping("/users/{users_Id}")
    public ResponseEntity<User> updateUser(@PathVariable Long users_Id, @RequestBody User userDetails){
        User user = userRepository.findById(users_Id)
                .orElseThrow(() -> new ResourceNotFoundException("User not exist with user_Id :" + users_Id));

        user.setUsername(userDetails.getUsername());
        user.setEmailId(userDetails.getEmailId());
        user.setPassword(userDetails.getPassword());

        User updatedUser = userRepository.save(user);
        return ResponseEntity.ok(updatedUser);
    }

    // delete delete
    @DeleteMapping("/users/{users_Id}")
    public ResponseEntity<Map<String, Boolean>> deleteEmployee(@PathVariable Long users_Id){
        User user = userRepository.findById(users_Id)
                .orElseThrow(() -> new ResourceNotFoundException("User not exist with user_Id :" + users_Id));

        userRepository.delete(user);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return ResponseEntity.ok(response);
    }
}
